import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';


import * as serviceWorker from './serviceWorker';


class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
     	users:[ { id: 1,
		     	  name: "raju",
			      age: 12,
			      city: "jaipur",
			      code: 302022,
			      country:"india"},

			    { id: 2,
			      name: "demo",
			      age: 14,
			      city: "new york",
			      code: 300001,
			      country:"UK"},

			     { id: 3,
			      name: "test",
			      age: 20,
			      city: "delhi",
			      code: 302022,
			      country:"india"},

			    { id: 4,
			      name: "Guru",
			      age: 30,
			      city: "dubai",
			      code: 302022,
			      country:"dubai"},

			    { id: 5,
			      name: "Rizz",
			      age: 12,
			      city: "jaipur",
			      code: 302022,
			      country:"india"}

      ],
    search:null
  };  	
}

delete(user){
    const newState = this.state.users.slice();
    if (newState.indexOf(user) > -1) {
    newState.splice(newState.indexOf(user), 1);
      this.setState({users: newState})
    }
 }

searchSpace=(event)=>{
    let keyword = event.target.value;
    this.setState({search:keyword})
  }




  render() {
  
  	const newUsers = this.state.users.filter((data)=>{
      if(this.state.search == null)
          return data
      else if(data.name.toLowerCase().includes(this.state.search.toLowerCase())  ){
          return data
      }
    }).map( user => { 
  		console.log(user);
  		return ( 

  			<ul> 
		        <li style={{backgroundColor: "lightblue"}}><h2 >My Name Is {user.name}</h2>
		        <p>
		          my age is  {user.age}.
		          my city is {user.city} and
		          postal code is {user.code}. 
		          my country is {user.country}.
		        </p></li>
		        <button 
	              onClick={this.delete.bind(this, user)}
	              className="btn btn-sm btn-danger ">
	              Delete
	            </button>
	        </ul>
  			);	
  	});
  	
     return (
	    <div>

	        <input style={{backgroundColor: "lightgray"}} type="text" placeholder="Enter name to be searched" onChange={(e)=>this.searchSpace(e)} />
	         
	        {newUsers}

	    </div>     
    );
  } 	
}

ReactDOM.render(<User />,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
